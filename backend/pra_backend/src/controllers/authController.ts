import catchAsync from "../utils/catchAsync";
import { NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';
import { AppError } from '../utils/appError';
import { userService } from '../service';
import { constVariable } from "../utils/const";
import { msg } from "../server";

const { JWT_SECRET } = require('../config/index');

const signToken = (userData: any) => {
      return jwt.sign({ userData }, JWT_SECRET, {
        expiresIn: constVariable.TOKENEXPIRE,
      });
  };


const auth = {
    login: catchAsync(async (req: any, res: any, next: NextFunction) => {
        const { email, password } = req.body;

        const user = await userService.findOne({ query : {email : email} });
        
        if (password != user.password) {
            return res.status(constVariable.HTTP.UNAUTHORIZED).json({ 
                status: msg.status.error,
                message: msg.controllers.auth.invalidCredential });
        }

        const token = signToken(user);

        return res.status(constVariable.HTTP.OK).json({
            status: msg.status.success,
            message: msg.controllers.auth.loginSuccessfull,
            token : token
        });
    }),

    protect: catchAsync(async (req: any, res: Response, next: NextFunction) => {
        // 1) Getting token and check of it's there
        if (!req.headers.authorization) {
          return next(new AppError(msg.controllers.auth.youAreNotLoggedIn, constVariable.HTTP.UNAUTHORIZED));
        }
        let token;
        if (req.headers.authorization && req.headers.authorization.startsWith(constVariable.HTTP.BEARER)) {
          token = req.headers.authorization.split(' ')[1];
        }
        else if (req.cookies.jwt) {
          token = req.cookies.jwt;
        }
    
        if (!token) {
          return next(new AppError(msg.controllers.auth.youAreNotLoggedIn, constVariable.HTTP.UNAUTHORIZED));
        }
        // 2) Verification token
        const decoded: any = await jwt.verify(token, JWT_SECRET);
        // 3) Check if user still exists
        const currentUser = await userService.findOne({ query: decoded.userData});
        if (!currentUser) {
          return next(new AppError(msg.controllers.auth.userBelongToThisTokenDeleted, constVariable.HTTP.UNAUTHORIZED));
        }
    
        // GRANT ACCESS TO PROTECTED ROUTE
        req.user = currentUser;
        next();
      }),
}

export default auth;