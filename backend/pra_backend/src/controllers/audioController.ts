import catchAsync from "../utils/catchAsync";
import { NextFunction } from 'express';
import { msg } from "../server";
import { constVariable } from "../utils/const";
import { audioService } from "../service";


const Audio = {

    addAudio: catchAsync(async (req: any, res: any, next: NextFunction) => {
        if(!req.file){
            return res.status(constVariable.HTTP.BADREQUEST).json({ 
                status: msg.status.error,
                message: msg.controllers.audio.fileNotfound });
        }

        req.body.audioUrl = req.file.path

        await audioService.createNewOne(req.body)

        return res.status(constVariable.HTTP.CREATED).json({ 
            status: msg.status.success,
            message: msg.controllers.audio.fileInserted })

    }),

    getAllAudio: catchAsync(async (req: any, res: any, next: NextFunction) => {
        const {page, limit} = req.query
        req.query.skip = (Number(page) - 1) * limit


        let result = await audioService.findList({ query: { }, skip: req.query.skip , limit: Number(limit) })

        return res.status(constVariable.HTTP.OK).json({ 
            status: msg.status.success,
            data:  result})
    }),

    deleteAudio: catchAsync(async (req: any, res: any, next: NextFunction) => {
        const audioExist = await audioService.findOne({query: { _id: req.params.id}})

        if(!audioExist){
            return res.status(constVariable.HTTP.NOTFOUND).json({ 
                status: msg.status.error,
                message: msg.controllers.audio.audioNotFound });
        }

        await audioService.deleteOne({ query: { _id: req.params.id}})

        return res.status(constVariable.HTTP.OK).json({ 
            status: msg.status.success,
            message: msg.controllers.audio.audioDelete })
    }),

    getOneAudio: catchAsync(async (req: any, res: any, next: NextFunction) => {
        const audioExist = await audioService.findOne({query: { _id: req.params.id}})

        if(!audioExist){
            return res.status(constVariable.HTTP.NOTFOUND).json({ 
                status: msg.status.error,
                message: msg.controllers.audio.audioNotFound });
        }

        return res.status(constVariable.HTTP.OK).json({ 
            status: msg.status.success,
            data : audioExist
        })
    }),

    updateAudio: catchAsync(async (req: any, res: any, next: NextFunction) => {

        const audioExist = await audioService.findOne({query: { _id: req.params.id}})

        if(!audioExist){
            return res.status(constVariable.HTTP.NOTFOUND).json({ 
                status: msg.status.error,
                message: msg.controllers.audio.audioNotFound });
        }

        if(req.file){
            await audioService.updateOne({ query :  { _id: req.params.id} , dataToUpdate: {audioUrl : ''}})
            req.body.audioUrl = req.file.path
            await audioService.updateOne({ query :  { _id: req.params.id} , dataToUpdate: req.body})
            return res.status(constVariable.HTTP.CREATED).json({ 
                status: msg.status.success,
                message: msg.controllers.audio.fileUpdate 
            })
        }

        await audioService.updateOne({ query :  { _id: req.params.id} , dataToUpdate: req.body})
        return res.status(constVariable.HTTP.CREATED).json({ 
            status: msg.status.success,
            message: msg.controllers.audio.fileUpdate 
        })

    })
}

export default Audio;