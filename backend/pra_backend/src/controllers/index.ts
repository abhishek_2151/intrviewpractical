import audioController from "./audioController"
import authController from "./authController"

export {
    audioController,
    authController
}

export default {
    audioController,
    authController
}
