import { DB_HOST,DB_PORT,DB_DATABASE} from '../config';

// this connection also use in agenda, handle with care
export const dbConnectionString =  
`mongodb://${DB_HOST}:${DB_PORT}/${DB_DATABASE}` 

export const dbConnection = {
  url: dbConnectionString,
  options: {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
  },
};
