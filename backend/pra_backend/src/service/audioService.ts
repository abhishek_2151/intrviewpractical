import { AudioModel } from '../models';
import { defaultListingParameter } from '../config/database';


export const createNewOne = async (inputData: any) => {
    return await AudioModel.create(inputData)
}

export const updateOne = async ({ query = defaultListingParameter.query, dataToUpdate = {}, options = { new: true } }) => {
    return await AudioModel.findOneAndUpdate(query, { $set: dataToUpdate }, options)
}

export const findList = async ({ 
            query = defaultListingParameter.query, 
            projection = defaultListingParameter.projection,
            sort = defaultListingParameter.sort,
            skip = defaultListingParameter.skip,
            limit = defaultListingParameter.limit,
        }) => {
            console.log(limit,"lomit")

        return AudioModel.find({...query }, projection).sort(sort)
            .skip(skip).limit(limit).lean()
}

export const deleteOne = async ({ query = defaultListingParameter.query }) => {
    return await AudioModel.deleteOne({...query })
}


export const findOne = async ({ query = defaultListingParameter.query, 
    projection = defaultListingParameter.projection}) => {
return await AudioModel.findOne({ active: true, ...query }, projection).lean()
}

export default {
    createNewOne,
    findList,
    findOne,
    deleteOne,
    updateOne
}
