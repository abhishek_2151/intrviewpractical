export const constVariable = {
    HTTP: {
        OK: 200,
        CREATED: 201,
        BADREQUEST : 409,
        UNAUTHORIZED: 401,
        NOTFOUND: 404,
        HTTPS: "https",
        XFORWARDEDPROTO: "x-forwarded-proto",
        BEARER: "Bearer",
    },
    TOKENEXPIRE: "7d",
    REGEX: {
        EMAIL: /^[a-zA-Z0-9][-a-zA-Z0-9._]+@([-a-zA-Z0-9]+[.])+[a-z]{2,5}$/,
    },
}
