import userValidator from "./user.validator";
import audioValidator from "./audio.validator";
export {
    userValidator,
    audioValidator
}

export default {
    userValidator,
    audioValidator
}
