import * as Joi from "joi";
import { msg } from '../server'
import { constVariable } from "../utils/const";

export const login = Joi.object({
    email: Joi.string().trim().required().pattern(new RegExp(constVariable.REGEX.EMAIL)).messages(msg.joi),
    password: Joi.string().trim().messages(msg.joi).allow(null, '')
})

export default {
    login
}