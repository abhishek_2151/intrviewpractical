import mongoose from 'mongoose';
import { constVariable } from '../utils/const';
import { databaseSchemaNames, schemaOptions } from '../config/database';
import { msg } from '../server'


const userSchema = new mongoose.Schema(
    {
      name: { type: String, required: [true, msg.module.insertName], index: true, trim: true },
      userName: { type: String, trim: true },
      email: {
        type: String, validate: {
          validator: function (v: any) {
            return constVariable.REGEX.EMAIL.test(v);
          },
          message: msg.module.insertValidEmail
        },
        trim: true
      },
      password: { type: String, trim: true },
    },
    schemaOptions,
  );

const User = mongoose.model(databaseSchemaNames.userSchema, userSchema);

export default User;