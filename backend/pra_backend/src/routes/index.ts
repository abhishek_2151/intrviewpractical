import express from 'express';
import { endPoints } from '../utils/endpoints';
import audioRouter from './audioRoutes';    
import userRouter from '../routes/authRoutes'

const router: express.Router = express.Router();

router.use(endPoints.user , userRouter);
router.use(endPoints.audio , audioRouter);


export default router
