import express from 'express';
import { authController } from '../controllers';
import { JoiValidator } from '../utils';
import { userValidator } from '../validator';

const router: any = express.Router();


router.post('/login', JoiValidator.validator.body(userValidator.login) ,authController.login)


export default router;
