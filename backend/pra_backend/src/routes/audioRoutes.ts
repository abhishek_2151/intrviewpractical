import express from 'express';
import { audioController } from '../controllers';
import { JoiValidator } from '../utils';
import { audioValidator } from '../validator';
import { upload } from '../middleware/uploadFile';
import { authController } from '../controllers';

const router: any = express.Router();


router.post('/add',[upload.single("audio")] ,JoiValidator.validator.body(audioValidator.addAudio) ,audioController.addAudio)

router.get('/',[authController.protect] , audioController.getAllAudio)

router.delete('/:id',[authController.protect] , audioController.deleteAudio)

router.get('/:id',[authController.protect] , audioController.getOneAudio)

router.put('/:id',[authController.protect] , JoiValidator.validator.body(audioValidator.updateAudio)  ,audioController.updateAudio)




export default router;
